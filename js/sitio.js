/**
 * Created by Edwin Pech on 13/05/2017.
 */

/*
* Iniciamos animacion WOW
* */
new WOW().init();

/*
    efecto flecha arriba suave
 */

smoothScroll.init({
   speed: 1000, //Valor entero, velocidad del scroll en milisegundos
    offset: 75,
});

/*---------------------------------
 OCULTAR Y MOSTRAR BOTON IR ARRIBA
 ----------------------------------*/
$(function () {
    $(window).scroll(function () {
        var scrolltop = $(this).scrollTop();
        if (scrolltop >= 50) {
            $(".arrow-up").fadeIn();
        } else {
            $(".arrow-up").fadeOut();
        }
    });

});

/*---------------------------------
 CABECERA ANIMADA
 ----------------------------------*/
$(window).scroll(function () {

    var nav = $('.encabezado');
    var scroll = $(window).scrollTop();

    if (scroll >= 80) {
        nav.addClass("fondo-menu");
    } else {
        nav.removeClass("fondo-menu");
    }
});

